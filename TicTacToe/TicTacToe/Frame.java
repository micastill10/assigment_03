package TicTacToe;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

 public class Frame extends JPanel {

   static final int WIDTH_IMAGEN = 40;
   static final int HIGTH_IMAGEN = 160;
   String[] img = { "", "/TicTacToe/Image/vic_nor.png", "/TicTacToe/Image/cor_nor.png", "/TicTacToe/Image/vic_enc.png", "/TicTacToe/Image/cor_enc.png" };
   int content;
   int pos;

   public Frame(int p)
   {
     this.content = 0;
     this.pos = p;
     setBackground(Color.white);
     setSize(60, 100);
   }

   public void setCont(int n) {
     this.content = n;
     repaint();
   }

   public void paint(Graphics g) {
     super.paintComponent(g);
     g.fillRect(0, 0, getWidth(), 2);
     g.fillRect(0, getHeight() - 2, getWidth(), 2);
     g.fillRect(0, 0, 2, getHeight());
     g.fillRect(getWidth() - 2, 0, 2, getHeight());

     g.drawImage(new ImageIcon(getClass().getResource(this.img[this.content])).getImage(), getWidth() / 2 - 20, getHeight() / 2 - 80, 40, 160, null);
   }
 }

