package TicTacToe;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

 public class ThreadTic implements Runnable {

   int[] spaces;
   Frame[] frame;
   int pos;
   int piece;
   static boolean process;
   Game g;
   String win;

   public ThreadTic(int[] spaces, Frame[] frame, Game g) {
     this.spaces = spaces;
     this.frame = frame;
     this.g = g;
     this.win = "";
   }
   public void setData(int pos, int piece) {
     this.pos = pos;
     this.piece = piece;
   }

   public void run() {
     int c = 0;
    process = true;
     while (c != 3) {
       for (int n = 0; n < this.spaces.length; n++) {
         if (this.spaces[n] == 0) {
           if (n == this.pos) {
             if (c == 3) {
               return;
            }
             c++;
           }

           this.frame[n].setCont(this.piece + 2);
           try
           {
             Thread.sleep(100L);
           } catch (InterruptedException ex) {
            Logger.getLogger(ThreadTic.class.getName()).log(Level.SEVERE, null, ex);
           }
           this.frame[n].setCont(0);
         }
       }
     }
     for (int n = 1; n < 6; n++) {
       this.frame[this.pos].setCont(0);
       try
       {
         Thread.sleep(200L);
       } catch (InterruptedException ex) {
         Logger.getLogger(ThreadTic.class.getName()).log(Level.SEVERE, null, ex);
       }

       this.frame[this.pos].setCont(this.piece);
       try {
         Thread.sleep(200L);
       } catch (InterruptedException ex) {
         Logger.getLogger(ThreadTic.class.getName()).log(Level.SEVERE, null, ex);
       }
     }

     this.frame[this.pos].setCont(this.piece);
     this.spaces[this.pos] = this.piece;
     process = false;
     if (!this.win.equals("")) {
       JOptionPane.showMessageDialog(null, this.win);
       Game.losing += 1;
       this.g.finish();
     }
     this.g.emp();
   }
 }

