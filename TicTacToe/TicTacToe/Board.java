package TicTacToe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

 public class Board extends JFrame implements MouseListener {

   static final int LENGTH = 450;
   static final int HIGH = 600;
   Frame[] frame;
   Game game;
   String name;
   JButton newtic;
   JPanel info;
   JPanel tab;
   JLabel j1;
   JLabel j2;
   JLabel j3;
   JLabel j4;

   public void UpdateLabels() {

     this.j2.setText("Won : " + Game.won);
     this.j3.setText("Lost: " + Game.losing);
     this.j4.setText("Tied: " + Game.tied);
   }

   public Board(String name) {

     super("Game to line - " + name + "   Vs  Machine");
     Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
     setSize(450, 600);
     setDefaultCloseOperation(3);
     setLocation(d.width / 2 - 225, d.height / 2 - 300);
     setVisible(true);
     setResizable(false);

    this.tab = new JPanel();
    this.info = new JPanel();

     this.info.setLayout(new GridLayout(0, 1));

     this.j1 = new JLabel(name + " Vs Machine");
     this.j1.setFont(new Font(this.j1.getFont().getFontName(), this.j1.getFont().getStyle(), 15));
     this.j1.setForeground(Color.BLUE);
     this.info.add(this.j1);

     this.j2 = new JLabel();
     this.j2.setFont(new Font(this.j2.getFont().getFontName(), this.j2.getFont().getStyle(), 15));
     this.j2.setForeground(Color.BLUE);
     this.j2.setHorizontalAlignment(0);
     this.info.add(this.j2);

     this.j3 = new JLabel();
     this.j3.setFont(new Font(this.j3.getFont().getFontName(), this.j3.getFont().getStyle(), 15));
     this.j3.setForeground(Color.BLUE);
     this.j3.setHorizontalAlignment(0);
     this.info.add(this.j3);

     this.j4 = new JLabel();
     this.j4.setFont(new Font(this.j4.getFont().getFontName(), this.j4.getFont().getStyle(), 15));
     this.j4.setHorizontalAlignment(0);
     this.j4.setForeground(Color.BLUE);
     this.info.add(this.j4);

     UpdateLabels();

     this.newtic = new JButton("New Game");
     this.newtic.setEnabled(false);
     this.newtic.addActionListener(new ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         Board.this.dispose();
         Main m = new Main();
       }
     });
    this.info.add(this.newtic);
     setLayout(new BorderLayout());
     add(this.info, "East");
     add(this.tab, "Center");

     this.tab.setLayout(new GridLayout(0, 3));

     this.frame = new Frame[9];
     for (int n = 0; n < 9; n++) {
       this.frame[n] = new Frame(n);
       this.frame[n].addMouseListener(this);
       this.tab.add(this.frame[n]);
     }
   }

   public void mouseClicked(MouseEvent e)
   {
     if ((this.game.finished) || (ThreadTic.process)) {
       return;
     }

     if (this.game.moveplayer(((Frame)e.getSource()).pos))
       this.game.evolution();
   }

   public void mousePressed(MouseEvent e)
   {
   }

   public void mouseReleased(MouseEvent e)
   {
   }

  public void mouseEntered(MouseEvent e)
   {
     int c = ((Frame)e.getSource()).content;
     int p = this.game.user.piece();

     if ((this.game.finished) || (ThreadTic.process)) {
       return;
     }
    if (c == 0)
       ((Frame)e.getSource()).setCont(p + 2);
   }

   public void mouseExited(MouseEvent e)
   {
     int c = ((Frame)e.getSource()).content;
     if (ThreadTic.process) {
       return;
     }
     if (c > 2)
       ((Frame)e.getSource()).setCont(0);
  }
}

