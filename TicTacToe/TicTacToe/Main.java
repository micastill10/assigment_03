package TicTacToe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import alice.tuprolog.*;

 public class Main extends JFrame implements ActionListener {

   static final int LENGH = 300;
   static final int HIGH = 400;
   JPanel p1;
   JPanel p2;
   JRadioButton rb1;
   JRadioButton rb2;
   ButtonGroup bg;
   static String name;
   String[] img = { "/TicTacToe/Image/vic_nor", "/TicTacToe/Image/cor_nor" };
   JButton[] bs;

   public void paint(Graphics g)
   {
     super.paintComponents(g);
   }

   public Main() {

     do
       name = JOptionPane.showInputDialog("Write you name", name);
     while ((name == null) || (name.equals("")));


     setTitle("Welcom: " + name);
     Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
     setSize(300, 400);
     setLocation(d.width / 2 - 150, d.height / 2 - 200);
     setVisible(true);
     setDefaultCloseOperation(3);
     setResizable(false);
     setLayout(new BorderLayout());

     this.bg = new ButtonGroup();
     this.rb1 = new JRadioButton("First");
     this.rb2 = new JRadioButton("Second", true);
     this.bg.add(this.rb1);
     this.bg.add(this.rb2);

     this.p1 = new JPanel();
     this.p1.setLayout(new GridLayout(0, 1));

     JPanel pt = new JPanel();
     pt.add(new JLabel("Choose you turn: "));
     this.p1.add(pt);

     JPanel p = new JPanel();
     p.add(this.rb1);
     p.add(this.rb2);
     this.p1.add(p);

     add(this.p1, "North");

     this.p2 = new JPanel();
     this.p2.setLayout(new BorderLayout());

     JPanel pca = new JPanel();
     pca.add(new JLabel("Choose you figure for play"));
     this.p2.add(pca, "North");

     JPanel pcc = new JPanel();
     this.bs = new JButton[2];
     for (int n = 0; n < this.bs.length; n++) {
       this.bs[n] = new JButton(new ImageIcon(getClass().getResource(this.img[n] + ".png")));
       this.bs[n].setActionCommand(n + "");
       this.bs[n].addActionListener(this);
       pcc.add(this.bs[n]);
    }

     this.p2.add(pcc);
     add(this.p2, "Center");
     repaint();
   }

   public static void main(String[] args) {
/* 122 */     Main m = new Main();
/*     */   }

   public void actionPerformed(ActionEvent e)
   {
     dispose();
     Prolog p = new Prolog();
     Board t = new Board(name);
     Game g = new Game(name, Integer.parseInt(e.getActionCommand()) + 1, this.rb1.isSelected() ? 1 : 2, t);
     t.game = g;
     p.setException(true);

   }
 }

