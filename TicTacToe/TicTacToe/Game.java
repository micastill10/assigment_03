package TicTacToe;

import javax.swing.JOptionPane;

 public class Game {

   ThreadTic tt;
   Player user;
   Player computer;
   Board frame;
   boolean finished;
   int[] speces = new int[9];
   static int losing;
   static int won;
   static int tied;

   public void emp()
   {
     if (this.finished) {
       return;
     }
     for (int n = 0; n < 9; n++) {
       if (this.speces[n] == 0) return;
     }

     tied += 1;
     finish();

     JOptionPane.showMessageDialog(null, "You have tied the game ");
   }

   public Game(String name, int pieceplayer, int turn, Board f) {
     this.tt = new ThreadTic(this.speces, f.frame, this);
     this.user = new Player(name, pieceplayer, turn);

     if (pieceplayer == 1)
       pieceplayer = 2;
     else {
       pieceplayer = 1;
     }
     if (turn == 1)
       turn = 2;
     else {
       turn = 1;
     }
     this.frame = f;

     this.computer = new Player("Machine", pieceplayer, turn);

     if (turn == 1)
       evolution();
   }

   public void evolution()
   {
     if (running(this.computer.piece(), true)) return;
     System.out.println("Play: A");
     if (running(this.user.piece(), false)) return;
     System.out.println("Play: B");
     if (move(this.computer.piece())) return;
     System.out.println("Play: C");
     tied += 1;
     finish();
     JOptionPane.showMessageDialog(null, "You have tied the game ");
   }

   public boolean moveplayer(int pos) {
     if (this.speces[pos] == 0) {
       this.speces[pos] = this.user.piece();
       this.frame.frame[pos].setCont(this.user.piece());
       return true;
     }
     return false;
   }

   public boolean move(int piece) {
     if (this.computer.turn() == 2) {
       if (!this.computer.first_go()) {
         //this.computer.firstgo() = true;

         if (this.speces[1] != 0) {
           this.tt.setData(2, piece);
           Thread t = new Thread(this.tt);
           t.start();
           return true;
         }
         if (this.speces[3] != 0) {
           this.tt.setData(0, piece);
           Thread t = new Thread(this.tt);
           t.start();
           return true;
         }
         if (this.speces[5] != 0) {
           this.tt.setData(2, piece);
           Thread t = new Thread(this.tt);
           t.start();
           return true;
         }
         if (this.speces[7] != 0) {
           this.tt.setData(6, piece);
           Thread t = new Thread(this.tt);
           t.start();
           return true;
         }
         if ((this.speces[0] == 0) && (this.speces[2] == 0) && (this.speces[6] == 0) && (this.speces[8] == 0)) {
           this.tt.setData(0, piece);
           Thread t = new Thread(this.tt);
           t.start();
           return true;
         }
        if (this.speces[4] == 0) {
           this.tt.setData(4, piece);
           Thread t = new Thread(this.tt);
           t.start();

           return true;
         }
       }
       else {
         for (int n = 1; n < 9; n += 2) {
           if (this.speces[n] == 0) {
             this.tt.setData(n, piece);
             Thread t = new Thread(this.tt);
             t.start();

            return true;
           }
         }
       }
     }

    if ((this.speces[1] == this.user.piece()) && (this.speces[7] == 0)) {
      this.tt.setData(7, piece);
       Thread t = new Thread(this.tt);
      t.start();
       return true;
     }

     if ((this.speces[3] == this.user.piece()) && (this.speces[2] == 0)) {
       this.tt.setData(2, piece);
       Thread t = new Thread(this.tt);
       t.start();
       return true;
     }

     if (this.speces[5] == this.user.piece()) {
       this.tt.setData(6, piece);
       Thread t = new Thread(this.tt);
       t.start();
       return true;
     }
     if (this.speces[7] == this.user.piece()) {
       this.tt.setData(2, piece);
       Thread t = new Thread(this.tt);
       t.start();
       return true;
     }

     for (int n = 0; n < 2; n++) {
       if (this.speces[(n * 2)] == 0) {
         this.tt.setData(n * 2, piece);
         Thread t = new Thread(this.tt);
         t.start();
         return true;
       }
       if (this.speces[(8 - n * 2)] == 0) {
         this.tt.setData(8 - n * 2, piece);
         Thread t = new Thread(this.tt);
         t.start();
         return true;
       }

     }

     return false;
   }

   public boolean running(int piece, boolean win) {
     for (int n = 0; n < 3; n++) {
      if (verificationSpaces(piece, n * 3, 1, win)) return true;
     }

     for (int n = 0; n < 3; n++) {
       if (verificationSpaces(piece, n, 3, win)) return true;
     }

     if (verificationSpaces(piece, 0, 4, win)) return true;

     if (verificationSpaces(piece, 2, 2, win)) return true;

     return false;
   }

   public boolean verificationSpaces(int piece, int start, int jump, boolean win)
   {
     int counter = 0;
     int pos = 0;
     int pieceOpposite;

     if (piece == 1)
       pieceOpposite = 2;
     else {
       pieceOpposite = 1;
     }
    for (int n = start; n < start + jump * 3; n += jump)
     {
       if (this.speces[n] == pieceOpposite) {
         return false;
       }

       if (this.speces[n] == 0) {
       if (counter == 1) {
           return false;
         }
         pos = n;
         counter++;
       }

     }

     if (win) {
       this.tt.setData(pos, piece);
       Thread t = new Thread(this.tt);
       t.start();
       String complement;

       if (this.computer.piece() == 2) {
         complement = "Star";
       }
       else {
         complement = "Circle";
       }

       this.tt.win = ("The " + this.computer.name() + " has the " + complement + ", lucky for the next.!! ");
     }
     else
     {
       this.tt.setData(pos, pieceOpposite);
       Thread t = new Thread(this.tt);
       t.start();
     }

     return true;
   }

   public void finish()
   {
     this.frame.UpdateLabels();
     this.finished = true;
     this.frame.newtic.setEnabled(this.finished);
   }
 }
